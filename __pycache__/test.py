import time
import msvcrt

start = time.time()

while True:
    if msvcrt.kbhit():
        msvcrt.getch()
        print(time.time() - start)
